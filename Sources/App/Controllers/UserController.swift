//
//  UserController.swift
//  App
//
//  Created by Mikhail Semerikov on 30/08/2019.
//

import Vapor

final class UserController {
    func index(_ req: Request) throws -> Future<[User]> {
        return User.query(on: req).all()
    }
    
    func create(_ req: Request) throws -> Future<User> {
        return try req.content.decode(User.self).flatMap { user in
            return user.save(on: req)
        }
    }
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(User.self).flatMap { user in
            return user.delete(on: req)
            }.transform(to: .ok)
    }
    
    // view with users
//    func list(_ req: Request) throws -> Future<View> {
//        return User.query(on: req).all().flatMap { users in
//            let data = ["userlist": users]
//            return try req.view().render("userview", data)
//        }
//    }
    
    // create a new user
//    func create(_ req: Request) throws -> Future<Response> {
//        return try req.content.decode(User.self).flatMap { user in
//            return user.save(on: req).map { _ in
//                return req.redirect(to: "users")
//            }
//        }
//    }
    
}


