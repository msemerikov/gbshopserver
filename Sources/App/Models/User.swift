//
//  User.swift
//  App
//
//  Created by Mikhail Semerikov on 30/08/2019.
//

import Foundation
import FluentPostgreSQL
import Vapor

final class User: PostgreSQLModel {
    var id: Int?
    
    let name: String?
    let login: String
    let password: String
    let lastname: String?
    let gender: String
    let email: String
    let cardNumber: String
    let bio: String
    
    init(name: String? = nil, login: String, password: String, lastname: String? = nil, id: Int? = nil, gender: String, email: String, cardNumber: String, bio: String) {
        self.name = name
        self.login = login
        self.password = password
        self.lastname = lastname
        self.id = id
        self.gender = gender
        self.email = email
        self.cardNumber = cardNumber
        self.bio = bio
    }
    
}

extension User: Content { }

extension User: Migration { }

extension User: Parameter { }
